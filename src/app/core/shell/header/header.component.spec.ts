// import { TestBed, ComponentFixture } from '@angular/core/testing';
// import {  HttpClientTestingModule } from '@angular/common/http/testing';
// import { Routes } from '@angular/router';
// import { RouterTestingModule } from '@angular/router/testing';
// import { HeaderComponent } from './header.component';
// import { Store, MemoizedSelector } from '@ngrx/store';
// import { NO_ERRORS_SCHEMA } from '@angular/core';
// import { MatDialog, MatInput, MatSnackBar } from '@angular/material';
// import { AuthService } from 'angular-6-social-login';
// import { LoginService } from '../../services/login.service';
// import { UserDetailService } from '../../services/userDetails.service';
// import { Router } from '@angular/router';
// import * as UserState from '../../../reducers/index';
// import { SetUser, RemoveUser } from 'src/app/core/store/action/userDetails.action';
// import { UiService } from '../../../shared/ui-service.service';
// import { provideMockStore, MockStore } from '@ngrx/store/testing';
// import { validateBasis } from '@angular/flex-layout';

// describe('HeaderComponent', () => {
//     let fixture: ComponentFixture<HeaderComponent>;
//     let mockStore: MockStore<UserState.State>;
//     let mockUserSelector: MemoizedSelector<UserState.State, {}>;
    
//     let mockValidate;
//     let mockDialog;
//     let mockAuthService;
//     let mockLoginService;
//     let mockUserDetailService;
//     let mockMatSnackBar;
//     let mockUserState;
    

//     beforeEach( () => {
//         mockDialog = {};
//         mockAuthService = {};
//         mockUserDetailService = {};        
//         mockMatSnackBar = {};
//         mockUserState = {
//             user: {
//                 id: '112212923389056657790',
//                 name: 'Ashish Nair',
//                 email: 'nairashish369@gmail.com',
//                 image: 'https://lh5.googleusercontent.com/-73NU02cebhI/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3rerX68HE4IZhV40QdpyUfKACaCIlQ/s96-c/photo.jpg',
//                 token: '',
//                 role: 'Standard',
//                 preference: {
//                     language: 'en',
//                     genre: [],
//                     theater: [],
//                 },
//                 rating: {
//                     movieId: '',
//                     rating: ''
//                 }
        
//             }
//         };

//         TestBed.configureTestingModule({
//             declarations: [HeaderComponent],
//             imports: [HttpClientTestingModule, RouterTestingModule],
//             providers: [
//                 {provide: MatDialog, useValue: mockDialog },
//                 {provide: AuthService, useValue: mockAuthService},
//                 {provide: UserDetailService, useValue: mockUserDetailService},
//                 {provide: MatSnackBar, useValue: mockMatSnackBar},
//                 {provide: UserState.userSelector, useValue: mockUserState},
//                 provideMockStore(),
//             ],
//             schemas: [NO_ERRORS_SCHEMA],
//         });


        
//         fixture = TestBed.createComponent(HeaderComponent);
//         fixture.componentInstance.validate = () => {
//             fixture.componentInstance.authFlag = true;            
//         };
//         mockStore = TestBed.get(Store);
//         //mockUserSelector = mockStore.overrideSelector(UserState.userSelector, mockUserState);        
//         mockUserSelector = mockUserState;
//         fixture.detectChanges();
//     });

//     it('Should create  the header component ', () => {
//         expect(true).toBe(true);
//     })
// })