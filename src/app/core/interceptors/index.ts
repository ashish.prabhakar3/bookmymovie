import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HomeInterceptor } from './home.interceptor' ;
import { LoaderInterceptor } from './loader.interceptor';

export const httpInterceptProviders = [
    { provide: HTTP_INTERCEPTORS, useClass: HomeInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true }
]