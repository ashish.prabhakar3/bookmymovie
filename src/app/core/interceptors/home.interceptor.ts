import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';

@Injectable()
export class HomeInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler) {
        // const authToken = "my-auth-token";        
        // const authReq = req.clone({
        //     setHeaders: {Authorization: authToken}
        // });
        return next.handle(req);
    }
}