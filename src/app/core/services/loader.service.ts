import { Injectable } from '@angular/core';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { Store, State } from '@ngrx/store';
import * as MovieState from '../../reducers/index';
import { LoaderDisplay, LoaderHide } from '../../home/store/actions/home.action';


@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  constructor(private store: Store<MovieState.State>) { }

  show() {
    this.store.dispatch(new LoaderDisplay(true));
  }

  hide() {
    this.store.dispatch(new LoaderHide(false));
  }


}
