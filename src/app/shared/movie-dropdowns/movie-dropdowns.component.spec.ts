import { TestBed, ComponentFixture } from '@angular/core/testing';
import {  MovieDropdownsComponent } from './movie-dropdowns.component';
import { HomeService } from 'src/app/home/services/home.service';
import { Component } from '@angular/compiler/src/core';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { IterableChangeRecord_ } from '@angular/core/src/change_detection/differs/default_iterable_differ';

describe('Movies Dropdowns Component', () => {
    let fixture: ComponentFixture<MovieDropdownsComponent>;
    let mockHomeService;
    let mockLanguageList;

    beforeEach( () => {
        mockHomeService = jasmine.createSpyObj(['getGenres']);
        mockLanguageList = [
            { id:1,  name: 'English'},
            { id:2,  name: 'Japanese'},
            { id:3,  name: 'Chinese'},
         ];
        TestBed.configureTestingModule({
            declarations: [MovieDropdownsComponent],
            providers: [{provide: HomeService, useValue: mockHomeService},
            {provide: mockLanguageList, useValue:mockLanguageList},
            ],
            schemas: [NO_ERRORS_SCHEMA],
        });
        fixture = TestBed.createComponent(MovieDropdownsComponent);
    });

    it('should create movie dropdowns component', () => {
        expect(true).toBe(true);
    });

    it('should have 3 languages in list', () => {
        fixture.detectChanges();
        expect(mockLanguageList.length).toBe(3);
    });

});