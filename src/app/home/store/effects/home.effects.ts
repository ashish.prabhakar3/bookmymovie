import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType, Effect} from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import * as homeAction from '../actions/home.action';
import { map, tap, mergeMap, catchError, switchMap, exhaustMap} from 'rxjs/operators';
import { of } from 'rxjs';
import { HomeService } from '../../services/home.service';
import { Store, State } from '@ngrx/store';
import * as MovieState from '../../../reducers/index';
import { LoadCrewCast, SetTheaters } from '../../store/actions/home.action';

@Injectable()
export class HomeEffects {
    constructor(private actions$: Actions, private homeService: HomeService, private store: Store<MovieState.State>){}

   @Effect() loadNowShowingMovies$ =  this.actions$.pipe(
        ofType<homeAction.SetNowPlayingMovies>(homeAction.EMovieActionTypes.SET_NOW_PLAYING_MOVIES),
        mergeMap(
            (action) => this.homeService.getNowShowingMoviesList(action.payload)
            .pipe(map(data => {
                return new homeAction.SetNowPlayingMoviesSuccess(data['results']);
            }),
            catchError(error => of(new homeAction.SetNowPlayingMoviesFailure(error)))
            )
        ),
        )

        @Effect() loadUpcomingMovies$ =  this.actions$.pipe(
            ofType<homeAction.SetUpcomingMovies>(homeAction.EMovieActionTypes.SET_UPCOMING_MOVIES),
            mergeMap(
                (action) => this.homeService.getUpcomingMoviesList(action.payload).pipe(map(data => {
                    return new homeAction.SetUpcomingMoviesSuccess(data['results']);                    
                }),
                catchError(error => of(new homeAction.SetUpcomingMoviesFailure(error)))
                )
            ),
            )

            @Effect() loadCastAndCrew$ =  this.actions$.pipe(
                ofType<homeAction.LoadCrewCast>(homeAction.EMovieActionTypes.LOAD_CREW_CAST),
                mergeMap(
                    (action) => this.homeService.getCastAndCrew(action.payload).pipe(map(data => {                        
                        return new homeAction.LoadCrewCastSuccess(data);                    
                    }),
                    catchError(error => of(new homeAction.LoadCrewCastFailure(error)))
                    )
                ),
                )

        }
