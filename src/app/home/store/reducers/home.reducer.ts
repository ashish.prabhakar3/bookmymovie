import { MovieActionTypes, EMovieActionTypes } from '../actions/home.action';
import { Movie } from '../../models/movie.model';
import { CastCrew } from '../../models/castcrew.model';
import { State } from '@ngrx/store';
import { Theater } from '../../models/theater.model';

export interface MoviesState {
    nowPlayingMovies: Movie[];
    upcomingMovies: Movie[];
    setTheaters: Theater[];
    castCrew: CastCrew[];
    isLoading: boolean;    
}

export const initialMovieState: MoviesState = {
    nowPlayingMovies: [],
    upcomingMovies: [],
    setTheaters: [],
    castCrew:[],
    isLoading: false
};

export function moviesReducer(state = initialMovieState, action: MovieActionTypes) {
    switch (action.type) {
        case EMovieActionTypes.SET_NOW_PLAYING_MOVIES: 
            return {...state}
        case EMovieActionTypes.SET_NOW_PLAYING_MOVIES_SUCCESS: {
            // const objOfMovies = action.payload.reduce((o, movie) => ({ ...o, [movie.id]: movie }), {});
            // const newMovies: { [key: number]: Movie } = { ...state.nowPlayingMovies, ...objOfMovies }; 
                       
            const newNowPlayingMovies: Movie[] = [...state.nowPlayingMovies];            
            newNowPlayingMovies.push(...action.payload);
            return {
                ...state,
                nowPlayingMovies: newNowPlayingMovies
            };
        }
        case EMovieActionTypes.SET_NOW_PLAYING_MOVIES_FAILURE: {
            // const objOfMovies = action.payload.reduce((o, movie) => ({ ...o, [movie.id]: movie }), {});
            // const newMovies: { [key: number]: Movie } = { ...state.nowPlayingMovies, ...objOfMovies };            
            return {
                ...state,
                error: action.payload
            };
        }
        case EMovieActionTypes.SET_UPCOMING_MOVIES: 

            return {...state}
        case EMovieActionTypes.SET_UPCOMING_MOVIES_SUCCESS: {
            // const objOfMovies = action.payload.reduce((o, movie) => ({ ...o, [movie.id]: movie }), {});
            // const newUpcomingMovies: { [key: number]: Movie } = { ...state.upcomingMovies, ...objOfMovies };
            const newUpcomingMovies: Movie[] = [...state.upcomingMovies];
            newUpcomingMovies.push(...action.payload);
            return {
                ...state,
                upcomingMovies: newUpcomingMovies
            };
        }
        case EMovieActionTypes.SET_UPCOMING_MOVIES_FAILURE: {
            // const objOfMovies = action.payload.reduce((o, movie) => ({ ...o, [movie.id]: movie }), {});
            // const newMovies: { [key: number]: Movie } = { ...state.nowPlayingMovies, ...objOfMovies };            
            return {
                ...state,
                error: action.payload
            };
        }
        case EMovieActionTypes.SET_CAST_AND_CREW_NOW: {

            
            const newNowPlayingMovieState = [...state.nowPlayingMovies ];
            //const newUpcomingMovieState = { ...state.upcomingMovies };

            if (state.nowPlayingMovies[action.payload.id]) {
                newNowPlayingMovieState[action.payload.id].casts = [...action.payload.casts];
                newNowPlayingMovieState[action.payload.id].crews = [...action.payload.crews];
            }
            // if (state.upcomingMovies[action.payload.id]) {
            //     newUpcomingMovieState[action.payload.id].casts = action.payload.cast;
            //     newUpcomingMovieState[action.payload.id].crews = action.payload.crew;
            // }
            //upcomingMovies: newUpcomingMovieState
            return {
                ...state,
                nowPlayingMovies: newNowPlayingMovieState
            };
        }
        case EMovieActionTypes.SET_THEATERS: {
            const newSetTheatersState = action.payload;
            return {
                ...state,
                setTheaters: newSetTheatersState
            };
        }
        case EMovieActionTypes.LOAD_CREW_CAST: 
            return {...state}
        case EMovieActionTypes.LOAD_CREW_CAST_SUCCESS: {
            const castCrewData = {"id": action.payload.id, "casts": action.payload.cast.slice(0,5), "crews": action.payload.crew.slice(0,5) };
               return {
                   ...state,
                   castCrew: [...state.castCrew, castCrewData ]
               };            
        }
        case EMovieActionTypes.LOAD_CREW_CAST_FAILURE: {
            return {
                ...state,
                error: action.payload
            };
        }   
        case EMovieActionTypes.LOADER_DISPLAY: {
            return {
                ...state,
                isLoading: action.payload
            }
        }     
        case EMovieActionTypes.LOADER_HIDE: {
            return {
                ...state,
                isLoading: action.payload
            }
        }     
        default:
            return state;
    }
}
