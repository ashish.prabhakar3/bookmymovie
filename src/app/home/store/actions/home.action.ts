import { Action } from '@ngrx/store';
import { Movie } from '../../models/movie.model';
import { CastCrew } from '../../models/castcrew.model';

export enum EMovieActionTypes {
    SET_NOW_PLAYING_MOVIES = '[ Movie ] Set now playing movies',
    SET_NOW_PLAYING_MOVIES_SUCCESS = '[ Movie ] Set now playing movies success',
    SET_NOW_PLAYING_MOVIES_FAILURE = '[ Movie ] Set now playing movies failure',
    SET_UPCOMING_MOVIES = '[ Movie ] Set up coming movies',
    SET_UPCOMING_MOVIES_SUCCESS = '[ Movie ] Set up coming movies success',
    SET_UPCOMING_MOVIES_FAILURE = '[ Movie ] Set up coming movies failure',
    SET_CAST_AND_CREW_NOW = '[ Movie ] Set Credits',
    SET_THEATERS = '[ Theaters ] Set Theaters',
    LOAD_CREW_CAST = '[Movie] Load Crew Cast',
    LOAD_CREW_CAST_SUCCESS = '[Movie] Load Crew Cast Success',
    LOAD_CREW_CAST_FAILURE = '[Movie] Load Crew Cast Failure',
    LOADER_DISPLAY = '[Movie] Display loader',
    LOADER_HIDE = '[Movie] Hide Loader'
}

export class SetNowPlayingMovies implements Action {
    readonly type = EMovieActionTypes.SET_NOW_PLAYING_MOVIES;

    constructor(public payload: number) { }
}
export class SetNowPlayingMoviesSuccess implements Action {
    readonly type = EMovieActionTypes.SET_NOW_PLAYING_MOVIES_SUCCESS;

    constructor(public payload: Movie[]) { }
}
export class SetNowPlayingMoviesFailure implements Action {
    readonly type = EMovieActionTypes.SET_NOW_PLAYING_MOVIES_FAILURE;

    constructor(public payload: Error) { }
}

export class SetUpcomingMovies implements Action {
    readonly type = EMovieActionTypes.SET_UPCOMING_MOVIES;

    constructor(public payload: number) { }
}
export class SetUpcomingMoviesSuccess implements Action {
    readonly type = EMovieActionTypes.SET_UPCOMING_MOVIES_SUCCESS;

    constructor(public payload: Movie[]) { }
}
export class SetUpcomingMoviesFailure implements Action {
    readonly type = EMovieActionTypes.SET_UPCOMING_MOVIES_FAILURE;

    constructor(public payload: Error) { }
}

export class SetCastAndCrewNow implements Action {
    readonly type = EMovieActionTypes.SET_CAST_AND_CREW_NOW;

    constructor(public payload: any) { }
}

export class SetTheaters implements Action {
    readonly type = EMovieActionTypes.SET_THEATERS;

     constructor(public payload: any) {}
}

export class LoadCrewCast implements Action {
    readonly type = EMovieActionTypes.LOAD_CREW_CAST;

      constructor(public payload: number){}  
}
export class LoadCrewCastSuccess implements Action {
    readonly type = EMovieActionTypes.LOAD_CREW_CAST_SUCCESS;

    constructor(public payload: any) { }
}
export class LoadCrewCastFailure implements Action {
    readonly type = EMovieActionTypes.LOAD_CREW_CAST_FAILURE;

    constructor(public payload: Error) { }
}

export class LoaderDisplay implements Action {
    readonly type = EMovieActionTypes.LOADER_DISPLAY;

    constructor(public payload: boolean){}
}
export class LoaderHide implements Action {
    readonly type = EMovieActionTypes.LOADER_HIDE;

    constructor(public payload: boolean){}
}
export type MovieActionTypes = SetNowPlayingMovies | SetNowPlayingMoviesSuccess | SetNowPlayingMoviesFailure | SetUpcomingMovies | SetUpcomingMoviesSuccess | SetUpcomingMoviesFailure | SetCastAndCrewNow | SetTheaters | LoadCrewCast | LoadCrewCastSuccess | LoadCrewCastFailure | LoaderDisplay | LoaderHide;
