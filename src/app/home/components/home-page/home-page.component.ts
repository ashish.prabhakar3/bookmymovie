import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy,
  ViewChild,
  EventEmitter,
  Output,
  ViewEncapsulation,
  ChangeDetectorRef,
  AfterViewInit
} from '@angular/core';
import { Store, State } from '@ngrx/store';
import { Subject, BehaviorSubject, Observable, of} from 'rxjs';
import { filter, debounce, tap } from 'rxjs/operators';
import * as MovieState from '../../../reducers/index';
import { LoaderDisplay } from '../../store/actions/home.action';
import { CdkVirtualScrollViewport, ScrollDispatcher } from '@angular/cdk/scrolling';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush  
})
export class HomePageComponent implements OnInit, AfterViewInit {
  @Input()
  nowPlayingList;

  @Input()
  upcomingList;

  @Input()
  theaterList;

  @Input()
  userPreference;

  @Output()
  getNewNowPlayingMovies: EventEmitter<number> = new EventEmitter<number>();

  @Output()
  getNewUpcomingMovies: EventEmitter<number> = new EventEmitter<number>();

  activeTabIndex = 0;
  nowPlayingMovieFetchedPageNum = 1;
  upcomingMoviesFetchedPageNum = 0;
  nowListCount = 1;
  upListCount = 1;
  nowListEnd:boolean = false;
  upListEnd:boolean = false;
  selectedLanguage = '';
  selectedGenre = '';
  languageList = [{ id: 'en', name: 'English' }, { id: 'ja', name: 'Japanese' }, { id: 'zh', name: 'Chinese' }];
  upcmEmitStatus:boolean = false;
  showLoader$: Observable<boolean> = of(false);
  @ViewChild(CdkVirtualScrollViewport) virtualScroll: CdkVirtualScrollViewport;

  constructor(private store: Store<MovieState.State>, private cdr: ChangeDetectorRef, private scrollDispatcher: ScrollDispatcher) {}

  ngOnInit() {
    this.store.select(MovieState.loaderSelector).subscribe(result => {      
      this.showLoader$ = of(result);
      this.cdr.detectChanges();
    });

    this.getNewNowPlayingMovies.emit(1);
    //this.getNewUpcomingMovies.emit(1); 
    
    
  }

    ngAfterViewInit(){      
      // .pipe(
      //   tap(event => console.log(this.virtualScroll.measureScrollOffset('top'), this.nowListEnd)))      
      this.scrollDispatcher.scrolled()
      .subscribe(event => {
        if(this.virtualScroll.measureScrollOffset('bottom') < 500  ){
          if(this.activeTabIndex == 0 && this.nowListEnd === false && this.nowPlayingList.length > 0){
            this.nowListCount++;
            this.nowListEnd = true;
            this.getNewNowPlayingMovies.emit(this.nowListCount);                      
          }else if(this.activeTabIndex == 1 && this.upListEnd === false && this.upcomingList.length > 0){
            this.upListCount++;
            this.upListEnd = true;
            this.getNewUpcomingMovies.emit(this.upListCount);                    
          }
        }else if(this.virtualScroll.measureScrollOffset('top') < 500  ){
          if(this.activeTabIndex == 0 && this.nowListEnd === true && this.nowPlayingList.length > 0){
            this.nowListEnd = false;     
          }else if(this.activeTabIndex == 1 && this.upListEnd === true && this.upcomingList.length > 0){
            this.upListEnd = false;
          }     
        }          
      });
    }

  trackMovie(index, movie, count) {
    if (movie) {
      return movie.id;
    } else {
      return -1;
    }
  }
  getMovies() {}

  tabChanged(event) {
    this.activeTabIndex = event;
    if(event === 1 && !this.upcmEmitStatus ){
      this.getNewUpcomingMovies.emit(1);
      this.upcmEmitStatus = true;
    }
  }

  getLanguage(lang) {
    this.selectedLanguage = lang;
  }

  getGenre(g) {
    this.selectedGenre = g;
  }

}
