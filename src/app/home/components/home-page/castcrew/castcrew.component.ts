import { Component, OnInit, Input, ChangeDetectionStrategy, ChangeDetectorRef, ViewRef } from '@angular/core';
import { Store, State } from '@ngrx/store';
import * as MovieState from '../../../../reducers/index';
import { SetNowPlayingMovies, SetUpcomingMovies, SetCastAndCrewNow, SetTheaters, LoadCrewCast } from '../../../store/actions/home.action';
import { CastCrew} from '../../../models/castcrew.model';
import {Observable } from 'rxjs';
import { of } from 'rxjs';
import { BASE_URL, TMDB_URLS } from '../../../../shared/config';

@Component({
  selector: 'app-castcrew',
  templateUrl: './castcrew.component.html',
  styleUrls: ['./castcrew.component.scss'],
})
// changeDetection: ChangeDetectionStrategy.OnPush  
export class CastcrewComponent implements OnInit {
@Input() id;
castCrewList: any = {};
dataReceived$:boolean = false;
imagesPath = TMDB_URLS.IMAGE_URL;
castCrewPath = TMDB_URLS.CAST_CREW_SMALL;



  constructor(private store: Store<MovieState.State>, private cdr: ChangeDetectorRef) { }

  ngOnInit() {
    
    this.store.select(MovieState.castCrewSelector).subscribe(result => {
      let castData = result.filter(res => res.id === this.id);
      if(castData.length > 0){
        this.castCrewList = castData[0];        
        if(! (this.cdr as ViewRef).destroyed) {
          this.cdr.detectChanges();        
        }
        
      }
      
    });
    
    this.store.dispatch(new LoadCrewCast(this.id));

  }

}
