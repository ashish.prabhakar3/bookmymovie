import { Cast } from './cast.model';
import { Crew } from './crew.model';

export interface CastCrew {
    id: number;
    casts: Cast[];
    crews: Crew[];
}
