import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Store, State } from '@ngrx/store';
import * as MovieState from '../../../reducers/index';
import * as UserState from '../../../reducers/index';
import { SetNowPlayingMovies, SetUpcomingMovies, SetCastAndCrewNow, SetTheaters } from '../../store/actions/home.action';
import { CastCrew} from '../../models/castcrew.model';

import { HomeService } from '../../services/home.service';
import { LoaderService } from '../../../core/services/loader.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],  
  changeDetection: ChangeDetectionStrategy.OnPush  
})

export class HomeComponent implements OnInit {
  nowPlayingMoviesList: any = [];
  upcomingMoviesList: any = [];
  genresList: any = [];
  theaterList: any = [];
  userPreference: any = [];


  constructor(
    private store: Store<MovieState.State>,
    private userStore: Store<UserState.State>,
    private homeService: HomeService, private loaderService: LoaderService, private cdr: ChangeDetectorRef
  ) {}

  ngOnInit() {
    //this.getNewSetofNowPlayingMovies(1);
    this.store.select(MovieState.nowPlayingMoviesSelector).subscribe(result => {      
      this.nowPlayingMoviesList = result;
    });

    this.store.select(MovieState.upcomingMovieSelector).subscribe(result => {
      this.upcomingMoviesList = result;
      
    });
    this.store.select(MovieState.theaterList).subscribe(result => {
      this.theaterList = Object.values(result);
    });
    this.userStore.select(UserState.userSelector).subscribe(result => {
      this.userPreference = result.preference;
    });



    this.genresList = this.homeService.getGenres();

  }

   getNewSetofNowPlayingMovies(page) {
     this.store.dispatch(new SetNowPlayingMovies(page));
    }

  getNewSetofUpComingMovies(page) {
    this.store.dispatch(new SetUpcomingMovies(page));
  }
}
